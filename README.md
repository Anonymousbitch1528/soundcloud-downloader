# SoundCloud Downloader

SoundCloud Downloader is a python script that can download from SoundCloud.

## Installation

Just clone it with git on your computer.

```bash
git clone https://gitlab.com/Anonymousbitch1528/soundcloud-downloader.git
```

## Usage

**Step 1: Run it**

```bash
Anonymous@anonymous:~$python3 downloader.py
```

**Step 2: Enter the link**

```bash
Anonymous@anonymous:~$python3 downloader.py
Enter your soundcloud URL: https://soundcloud.com/eminemofficial/the-monster
```

**Step 3: Enter the path**

```bash
Anonymous@anonymous:~$python3 downloader.py
Enter your soundcloud URL: https://soundcloud.com/eminemofficial/the-monster
Enter the path that you want save your file: The-monster.mp3
```

**Step 4: Enjoy it**