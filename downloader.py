import requests
import json
from clint.textui import progress

def get_api_url(url):
    text1 = requests.get(url)
    text1 = text1.text.split('\n')
    for line in text1:
        if 'api-v2' in line and 'hls' in line:
            links = line.split('transcodings":')[1].split('},"monetization_model"')[0]
            link = json.loads(links)
            finallink = link[0]['url'] + '?client_id=pSgpripQRwObQBgmr7GR2RwCat351p2y'
            return finallink

def download_link_generator(url):
    api_link = get_api_url(url)
    text1 = requests.get(api_link).text
    link1 = json.loads(text1)['url']
    text2 = requests.get(link1).text.splitlines()[-2]
    downloadable_link = text2.split('media/')[0] + 'media/0/' + text2.split('media/')[1].split('/',1)[1]
    return downloadable_link

def downloader(url,path):
    r = requests.get(url, stream=True)
    with open(path, 'wb') as f:
        total_length = int(r.headers.get('content-length'))
        for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1): 
            if chunk:
                f.write(chunk)
                f.flush()

      
url = input('Enter your soundcloud URL: ')
path = input('Enter the path that you want save your file: ')
link = download_link_generator(url)
downloader(link,path)
print('Done.')
